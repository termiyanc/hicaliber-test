<?php

use App\Models\House;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index', [
        // Данные, которые будут переданы в приложение Vue
        'fields' => House::FIELDS,
        'elementUIFormData' => House::getElementUIFormData(),
        'all' => House::all()
    ]);
});
