<?php

use App\Models\House;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('search', function (Request $request) {
    // Поиск по модели House, дифференцированный по типу поиска
    $searchQuery = House::query();
    foreach (House::FIELDS as $field) {
        if (($value = $request->get($field['name'])) !== null) {
            switch (Arr::get($field, 'searchType')) {
                case 'nonStrict':
                    $searchQuery->where($field['name'], 'like', "%$value%");
                    break;
                case 'range':
                    $searchQuery->whereBetween($field['name'], $value);
                    break;
                default:
                    $searchQuery->where($field['name'], $value);
                    break;
            }
        }
    }
    return $searchQuery->get();
});
