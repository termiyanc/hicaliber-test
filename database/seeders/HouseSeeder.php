<?php

namespace Database\Seeders;

use App\Models\House;
use Illuminate\Database\Seeder;

/**
 * Class HouseSeeder
 * Производит импорт данных в модель App\Models\House из csv-файла storage/app/inner/houses.csv
 * @package Database\Seeders
 */
class HouseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Автор не стал выносить данный парсинг csv в переиспользуемую функцию,
        // а также определять в переменную путь к файлу, разделитель и прочие параметры, которые могли бы быть универсальными,
        // так как на данном этапе эти действия производятся единственный раз с конкретным известным файлом
        $file = fopen(storage_path('app/inner/houses.csv'), 'r');
        $lineNumber = 0;
        while (($dataLine = fgets($file)) !== false) {
            $lineNumber++;
            $data = explode(',', trim($dataLine));
            // В первой строке - поля,
            // которые для соответствия с именованием полей в базе данных приводятся к нижнему регистру
            if ($lineNumber == 1) {
                $fields = array_map(function ($fieldName) {
                    return mb_convert_case($fieldName, MB_CASE_LOWER);
                }, $data);
            } else {
                $house = new House();
                foreach ($fields as $index => $field) {
                    $house->setAttribute($field, $data[$index]);
                }
                $house->save();
            }
        }
    }
}
