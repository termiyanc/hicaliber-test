<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <title>Поиск домов</title>
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
</head>
<body>
<div id="search">
    <el-form :inline="true" :model="form">
        <el-form-item v-for="field in fields" :label="field.label">
            {{-- Вывод поля диапазона двумя полями ввода: от и до --}}
            {{-- Компонент "Slider" не используется, так как не воспринимает дробные числа --}}
            <template v-if="field.searchType == 'range'">
                <el-form-item class="range-input-from">
                    <el-input v-model="form[field.name]['from']"></el-input>
                </el-form-item>
                <el-form-item>
                    <el-input v-model="form[field.name]['to']"></el-input>
                </el-form-item>
            </template>
            <el-input v-else v-model="form[field.name]"></el-input>
        </el-form-item>
        <el-form-item>
            <el-button type="primary" @click="onSubmit">Искать</el-button>
        </el-form-item>
    </el-form>
    <el-divider></el-divider>
    <div>
        <vue-element-loading :active="loading"></vue-element-loading>
        <el-table :data="table">
            <template>
                <el-table-column v-for="field in fields" :prop="field.name" :label="field.label"></el-table-column>
            </template>
        </el-table>
    </div>
</div>
<script src="{{ mix('js/app.js') }}"></script>
<script>
    new Vue({
        el: '#search',
        data() {
            return {
                // Общее описание полей модели
                fields: @json($fields),
                // Данные формы поиска по модели
                form: @json($elementUIFormData),
                // Содержимое таблицы результатов поиска
                table: @json($all),
                // Признак состояния обработки поискового запроза и загрузки данных,
                // порождает вывод индикатора загрузки поверх таблицы результатов
                loading: false
            }
        },
        methods: {
            onSubmit() {
                this.loading = true;
                axios.post('{{ url('api/search') }}', this.form).then(response => {
                    this.table = response.data;
                    this.loading = false;
                });
            }
        }
    })
</script>
</body>
</html>
