<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class House extends Model
{
    /**
     * Описание выходных полей поиска и вывода данных модели, в формате:
     * [
     *      [
     *          'name' => string Имя поля
     *          'label' => string Выводимое название поля
     *          'searchType' => string Тип поиска по полю: 'nonStrict' - нестрогий поиск,
     *                                                     'range' - диапазон,
     *                                                     null (отсутствие значения) - строгий поиск по умолчанию
     *      ],
     *      ...
     * ]
     *
     * Семантика поисковых полей и колонок смешана, чтобы не порождать избыточную декларативность
     */
    public const FIELDS = [
        [
            'name' => 'name',
            'label' => 'Название',
            'searchType' => 'nonStrict'
        ],
        [
            'name' => 'price',
            'label' => 'Цена',
            'searchType' => 'range'
        ],
        [
            'name' => 'bedrooms',
            'label' => 'Спальни',
        ],
        [
            'name' => 'bathrooms',
            'label' => 'Ванные комнаты',
        ],
        [
            'name' => 'storeys',
            'label' => 'Этажи'
        ],
        [
            'name' => 'garages',
            'label' => 'Гаражи'
        ]
    ];

    /**
     * Возвращает поля формы для Element UI
     * @return array
     */
    public static function getElementUIFormData() {
        $form = [];
        foreach (static::FIELDS as $field) {
            if (Arr::get($field, 'searchType') == 'range') {
                // Для диапазона определим мин. и мак. значения
                $form[$field['name']] = [];
                $form[$field['name']]['from'] = static::min($field['name']);
                $form[$field['name']]['to'] = static::max($field['name']);
            } else {
                $form[$field['name']] = '';
            }
        }
        return $form;
    }
}
